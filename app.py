import os
import csv
import random
from dotenv import load_dotenv
import telebot
from telebot import types
import time

dotenv_path = './.env'
load_dotenv(dotenv_path)

# Read the CSV file
with open('quiz.csv', 'r') as csvfile:
    reader = csv.DictReader(csvfile)
    # Load the questions, answers, and categories into a dictionary
    questions = {}
    for row in reader:
        questions[row['question']] = {'answer': row['answer'].lower(), 'category': row['category']}

user_scores = {}
user_categories = {}

# Load the visitor statistics from the CSV file
visitor_stats = {'total_visitors': 0, 'unique_visitors': set()}
try:
    with open('visitor_stats.csv', 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            visitor_stats['total_visitors'] = int(row['total_visitors'])
            visitor_stats['unique_visitors'] = set(int(chat_id) for chat_id in row['unique_visitors'].split(','))
except FileNotFoundError:
    # Create the visitor_stats.csv file if it doesn't exist
    with open('visitor_stats.csv', 'w', newline='') as csvfile:
        fieldnames = ['total_visitors', 'unique_visitors']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
# Create an instance of the TeleBot class
bot_token = os.getenv("BOT_TOKEN")
bot = telebot.TeleBot(bot_token)

# Handle the "/start" command
@bot.message_handler(commands=['start'])
def start_quiz(message):
 # Update the visitor statistics
   # Update the visitor statistics
    visitor_stats['total_visitors'] += 1
    visitor_stats['unique_visitors'].add(message.chat.id)

    # Save the visitor statistics to the CSV file
    with open('visitor_stats.csv', 'w', newline='') as csvfile:
        fieldnames = ['total_visitors', 'unique_visitors']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerow({'total_visitors': visitor_stats['total_visitors'], 'unique_visitors': ','.join(str(chat_id) for chat_id in visitor_stats['unique_visitors'])})
   
    # Create a keyboard with the menu buttons
    keyboard = types.ReplyKeyboardMarkup(row_width=3, resize_keyboard=True)
    start_button = types.KeyboardButton("Start")
    help_button = types.KeyboardButton("Help")
    stats_button = types.KeyboardButton("Stats")
    leaderboard_button = types.KeyboardButton("Leaderboard")
    keyboard.add(start_button, help_button, stats_button, leaderboard_button)

    # Send the menu message to the user
    bot.send_message(chat_id=message.chat.id, text="Welcome to the quiz! Please select an option:", reply_markup=keyboard)

    # Store the user's state
    user_categories[message.chat.id] = {'state': 'menu'}

# Handle the menu options
@bot.message_handler(func=lambda message: user_categories.get(message.chat.id, {}).get('state') == 'menu')
def handle_menu_options(message):
    if message.text == "Start":
        # Get the unique categories from the questions
        categories = set(q_data['category'] for q_data in questions.values())

        # Create a keyboard with the categories and a main menu button
        keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
        buttons = [types.KeyboardButton(category) for category in categories]
        main_menu_button = types.KeyboardButton("Main Menu")
        keyboard.add(*buttons, main_menu_button)

        # Send the category selection message to the user
        bot.send_message(chat_id=message.chat.id, text="Choose a category:", reply_markup=keyboard)

        # Store the user's state
        user_categories[message.chat.id] = {'state': 'category_selection'}

    elif message.text == "Help":
        display_help(message)

    elif message.text == "Leaderboard":
        display_leaderboard(message)
        
    elif message.text == "Stats":
        display_visitor_stats(message)

    elif message.text == "Main Menu":
        start_quiz(message)

# Handle the category selection
@bot.message_handler(func=lambda message: user_categories.get(message.chat.id, {}).get('state') == 'category_selection')
def handle_category_selection(message):
    if message.text == "Main Menu":
        start_quiz(message)
        return

    selected_category = message.text

    # Create a keyboard with the number of questions options
    keyboard = types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    buttons = [types.KeyboardButton(str(i)) for i in [10, 20, 100, "Infinity"]]
    main_menu_button = types.KeyboardButton("Main Menu")
    keyboard.add(*buttons, main_menu_button)

    # Store the selected category and update the user's state
    user_categories[message.chat.id] = {'state': 'question_selection', 'category': selected_category}

    # Send the question selection message to the user
    bot.send_message(chat_id=message.chat.id, text=f"You selected the {selected_category} category. How many questions do you want?", reply_markup=keyboard)

# Handle the question selection
@bot.message_handler(func=lambda message: user_categories.get(message.chat.id, {}).get('state') == 'question_selection')
def handle_question_selection(message):
    if message.text == "Main Menu":
        start_quiz(message)
        return

    if message.text == "Infinity":
        num_questions = float("inf")
    else:
        num_questions = int(message.text)
    selected_category = user_categories[message.chat.id]['category']

    # Get the questions for the selected category
    category_questions = [q for q, q_data in questions.items() if q_data['category'] == selected_category]
    selected_questions = random.sample(category_questions, min(num_questions, len(category_questions)))

    # Start the quiz
    user_scores[message.chat.id] = {'score': 0, 'category_scores': {selected_category: 0}}
    for question in selected_questions:
        question_data = questions[question]
        user_scores[message.chat.id]['correct_answer'] = question_data['answer']
        user_categories[message.chat.id]['current_category'] = selected_category
        user_categories[message.chat.id]['current_question_index'] = 0

        bot.send_message(chat_id=message.chat.id, text=question)

        # Reset the user's state
        user_categories[message.chat.id]['state'] = 'answer_question'
        user_categories[message.chat.id]['start_time'] = time.time()

# Handle the user's answer
@bot.message_handler(func=lambda message: user_categories.get(message.chat.id, {}).get('state') == 'answer_question')
def check_answer(message):
    # Get the user's answer
    answer = message.text.lower()

    # Get the correct answer, current category, and start time from the user's data
    user_data = user_scores.get(message.chat.id)
    user_category_data = user_categories.get(message.chat.id)
    correct_answer = user_data.get("correct_answer") if user_data is not None else None
    current_category = user_category_data.get("current_category") if user_category_data is not None else None
    start_time = user_category_data.get("start_time") if user_category_data is not None else None

    if correct_answer is None or current_category is None or start_time is None:
        return

    # Check if the user's answer is correct (case-insensitive)
    if answer == correct_answer.lower():
        # Send a message to the user indicating that their answer was correct
        bot.send_message(chat_id=message.chat.id, text="Correct! The answer is in the {} category.".format(current_category))

        # Update the user's overall score and category-specific score in the dictionary
        user_data["score"] += 3
        user_data["category_scores"][current_category] = user_data["category_scores"].get(current_category, 0) + 3
    else:
        # Send a message to the user indicating that their answer was incorrect
        bot.send_message(chat_id=message.chat.id, text="Incorrect. The correct answer is {}.".format(correct_answer))

    # Get a new question from the dictionary, ensuring it's in the same category
    new_question, new_question_data = next((q, q_data) for q, q_data in questions.items() if q_data['category'] == current_category)

    # Send the new question to the user
    bot.send_message(chat_id=message.chat.id, text=new_question)

    # Store the correct answer and category in the user's data
    user_data["correct_answer"] = new_question_data['answer']
    user_category_data["current_category"] = new_question_data['category']
    user_category_data["current_question_index"] += 1

 # Handle the user's answer (continued)
    if user_category_data["current_question_index"] == int(user_category_data["current_question_index"]):
        # The user has answered all the questions, display the final score
        final_score = user_data["score"]
        bot.send_message(chat_id=message.chat.id, text=f"Congratulations! Your final score is {final_score}.")

        # Update the leaderboard
        update_leaderboard(message.chat.id, message.chat.username, final_score)

        # Reset the user's state
        user_categories[message.chat.id] = {'state': 'menu'}

    # Reset the timer
    user_category_data["start_time"] = time.time()

def display_help(message):
    help_text = """
    Here are the available commands:
    /start - Start the quiz
    "Start" - Start the quiz
    "Help" - Display the available commands
    "Leaderboard" - Display the leaderboard
    "Main Menu" - Go back to the main menu
    """
    bot.send_message(chat_id=message.chat.id, text=help_text)

def display_leaderboard(message):
    # Load the leaderboard data from a file or database
    leaderboard = load_leaderboard()

    # Create a message with the leaderboard
    leaderboard_message = "Leaderboard:\n\n"
    for rank, (username, score) in enumerate(leaderboard, 1):
        leaderboard_message += f"{rank}. {username} - {score}\n"

    # Send the leaderboard message to the user
    bot.send_message(chat_id=message.chat.id, text=leaderboard_message)

def load_leaderboard():
    # Load the leaderboard data from a file or database
    # Example implementation using a CSV file
    leaderboard = {}
    if os.path.exists("leaderboard.csv"):
        with open("leaderboard.csv", "r") as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                if len(row) == 2:
                    username, score = row
                    if username in leaderboard:
                        leaderboard[username] = max(leaderboard[username], int(score))
                    else:
                        leaderboard[username] = int(score)
    return sorted(leaderboard.items(), key=lambda x: x[1], reverse=True)

def update_leaderboard(chat_id, username, score):
    # Update the leaderboard data in a file or database
    # Example implementation using a CSV file
    leaderboard = load_leaderboard()
    leaderboard.append((username, score))
    leaderboard = sorted(leaderboard, key=lambda x: x[1], reverse=True)[:10]  # Keep the top 10 scores

    with open("leaderboard.csv", "w", newline="") as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(leaderboard)

def display_visitor_stats(message):
    total_visitors = visitor_stats['total_visitors']
    unique_visitors = len(visitor_stats['unique_visitors'])
    stats_message = f"Visitor statistics:\nTotal visitors: {total_visitors}\nUnique visitors: {unique_visitors}"
    bot.send_message(chat_id=message.chat.id, text=stats_message)
# Start the bot
bot.polling()